#!/bin/bash

expkey="202407091714"

pushd /var/nfs_dir

echo "10.0.0.10 slots=1" > hosts
echo "10.0.0.11 slots=1" >> hosts

source ~/spack/share/spack/setup-env.sh

PERFDIR=".starpu-aws-07-2024"
mkdir -p $PERFDIR

# single
mpirun --bind-to board -np 2 --machinefile $PWD/hosts -x STARPU_PERF_MODEL_DIR="$PWD/$PERFDIR" -x STARPU_SCHED=dmdas -x STARPU_CALIBRATE_MINIMUM=100 -x STARPU_CALIBRATE=1 `spack location -i chameleon`/bin/chameleon_stesting -o gemm -n 19200 -b 960 --threads=19 --gpus=4 --niter 10 -P 1 &> calibration-single-$expkey

# double
mpirun --bind-to board -np 2 --machinefile $PWD/hosts -x STARPU_PERF_MODEL_DIR="$PWD/$PERFDIR" -x STARPU_SCHED=dmdas -x STARPU_CALIBRATE_MINIMUM=100 -x STARPU_CALIBRATE=1 `spack location -i chameleon`/bin/chameleon_dtesting -o gemm -n 19200 -b 960 --threads=19 --gpus=4 --niter 10 -P 1 &> calibration-double-$expkey

# single check
mpirun --bind-to board -np 2 --machinefile $PWD/hosts -x STARPU_PERF_MODEL_DIR="$PWD/$PERFDIR" -x STARPU_SCHED=dmdas -x STARPU_CALIBRATE_MINIMUM=100 -x STARPU_CALIBRATE=1 `spack location -i chameleon`/bin/chameleon_stesting -o gemm -n 19200 -b 960 --threads=19 --gpus=4 --niter 2 -P 1 -c

# double check
mpirun --bind-to board -np 2 --machinefile $PWD/hosts -x STARPU_PERF_MODEL_DIR="$PWD/$PERFDIR" -x STARPU_SCHED=dmdas -x STARPU_CALIBRATE_MINIMUM=100 -x STARPU_CALIBRATE=1 `spack location -i chameleon`/bin/chameleon_dtesting -o gemm -n 19200 -b 960 --threads=19 --gpus=4 --niter 2 -P 1 -c



# test
for sz in 24960 28800 67200; do 
    for rep in `seq 5`; do
	echo " gemm  $rep <<";
	# 19 4 P1
	./run-script-single-prec.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o gemm -c 19 -g 4 -b 960 -n $sz -v -z -p 1

	# 19 4 P2
	./run-script-single-prec.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o gemm -c 19 -g 4 -b 960 -n $sz -v -z -p 2

	# 1 1 P1
	./run-script-single-prec.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o gemm -c 1 -g 1 -b 960 -n $sz -v -z -p 1

	# 1 1 P2
	./run-script-single-prec.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o gemm -c 1 -g 1 -b 960 -n $sz -v -z -p 2

	# 19 0 P1
	./run-script-single-prec.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o gemm -c 19 -g 0 -b 960 -n $sz -v -z -p 1

	# 19 0 P2
	./run-script-single-prec.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o gemm -c 19 -g 0 -b 960 -n $sz -v -z -p 2
	
	# 23 0 P1
	./run-script-single-prec.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o gemm -c 23 -g 0 -b 960 -n $sz -v -z -p 1

	# 23 0 P2
	./run-script-single-prec.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o gemm -c 23 -g 0 -b 960 -n $sz -v -z -p 2
    done;
done;



# test
for sz in 24960 28800 67200; do 
    for rep in `seq 5`; do
	echo "aqui gemm  $rep <<";
	# 19 4 P1
	./run-script.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o gemm -c 19 -g 4 -b 960 -n $sz -v -z -p 1

	# 19 4 P2
	./run-script.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o gemm -c 19 -g 4 -b 960 -n $sz -v -z -p 2

	# 1 1 P1
	./run-script.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o gemm -c 1 -g 1 -b 960 -n $sz -v -z -p 1

	# 1 1 P2
	./run-script.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o gemm -c 1 -g 1 -b 960 -n $sz -v -z -p 2

	# 19 0 P1
	./run-script.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o gemm -c 19 -g 0 -b 960 -n $sz -v -z -p 1

	# 19 0 P2
	./run-script.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o gemm -c 19 -g 0 -b 960 -n $sz -v -z -p 2
	
	# 23 0 P1
	./run-script.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o gemm -c 23 -g 0 -b 960 -n $sz -v -z -p 1

	# 23 0 P2
	./run-script.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o gemm -c 23 -g 0 -b 960 -n $sz -v -z -p 2	
    done;
done;


# trace
STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script-single-prec.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o gemm -c 19 -g 4 -b 960 -n 67200 -v -z -p 1 -t
STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script-single-prec.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o gemm -c 19 -g 4 -b 960 -n 67200 -v -z -p 2 -t

STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script-single-prec.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o gemm -c 1 -g 1 -b 960 -n 67200 -v -z -p 1 -t
STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script-single-prec.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o gemm -c 1 -g 1 -b 960 -n 67200 -v -z -p 2 -t

STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script-single-prec.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o gemm -c 19 -g 0 -b 960 -n 67200 -v -z -p 1 -t
STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script-single-prec.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o gemm -c 19 -g 0 -b 960 -n 67200 -v -z -p 2 -t

STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script-single-prec.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o gemm -c 23 -g 0 -b 960 -n 67200 -v -z -p 1 -t
STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script-single-prec.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o gemm -c 23 -g 0 -b 960 -n 67200 -v -z -p 2 -t

#
STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o gemm -c 19 -g 4 -b 960 -n 67200 -v -z -p 1 -t
STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o gemm -c 19 -g 4 -b 960 -n 67200 -v -z -p 2 -t

STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o gemm -c 1 -g 1 -b 960 -n 67200 -v -z -p 1 -t
STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o gemm -c 1 -g 1 -b 960 -n 67200 -v -z -p 2 -t

STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o gemm -c 19 -g 0 -b 960 -n 67200 -v -z -p 1 -t
STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o gemm -c 19 -g 0 -b 960 -n 67200 -v -z -p 2 -t

STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o gemm -c 23 -g 0 -b 960 -n 67200 -v -z -p 1 -t
STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o gemm -c 23 -g 0 -b 960 -n 67200 -v -z -p 2 -t
