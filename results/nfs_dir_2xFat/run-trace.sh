#!/bin/bash

expkey="202407091714"

pushd /var/nfs_dir

echo "10.0.0.10 slots=1" > hosts
echo "10.0.0.11 slots=1" >> hosts

source ~/spack/share/spack/setup-env.sh

PERFDIR=".starpu-aws-07-2024"
mkdir -p $PERFDIR

# trace potrf
## single
STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script-single-prec.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o potrf -c 19 -g 4 -b 960 -n 67200 -v -p 1 -t
STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script-single-prec.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o potrf -c 19 -g 4 -b 960 -n 67200 -v -p 2 -t

STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script-single-prec.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o potrf -c 1 -g 1 -b 960 -n 67200 -v -p 1 -t
STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script-single-prec.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o potrf -c 1 -g 1 -b 960 -n 67200 -v -p 2 -t

STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script-single-prec.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o potrf -c 19 -g 0 -b 960 -n 67200 -v -p 1 -t
STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script-single-prec.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o potrf -c 19 -g 0 -b 960 -n 67200 -v -p 2 -t

STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script-single-prec.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o potrf -c 23 -g 0 -b 960 -n 67200 -v -p 1 -t
STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script-single-prec.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o potrf -c 23 -g 0 -b 960 -n 67200 -v -p 2 -t

## double
STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o potrf -c 19 -g 4 -b 960 -n 67200 -v -p 1 -t
STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o potrf -c 19 -g 4 -b 960 -n 67200 -v -p 2 -t

STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o potrf -c 1 -g 1 -b 960 -n 67200 -v -p 1 -t
STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o potrf -c 1 -g 1 -b 960 -n 67200 -v -p 2 -t

STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o potrf -c 19 -g 0 -b 960 -n 67200 -v -p 1 -t
STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o potrf -c 19 -g 0 -b 960 -n 67200 -v -p 2 -t

STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o potrf -c 23 -g 0 -b 960 -n 67200 -v -p 1 -t
STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o potrf -c 23 -g 0 -b 960 -n 67200 -v -p 2 -t

# trace getrf
## single
STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script-single-prec.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o getrf -c 19 -g 4 -b 960 -n 67200 -v -p 1 -t
STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script-single-prec.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o getrf -c 19 -g 4 -b 960 -n 67200 -v -p 2 -t

STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script-single-prec.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o getrf -c 1 -g 1 -b 960 -n 67200 -v -p 1 -t
STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script-single-prec.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o getrf -c 1 -g 1 -b 960 -n 67200 -v -p 2 -t

STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script-single-prec.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o getrf -c 19 -g 0 -b 960 -n 67200 -v -p 1 -t
STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script-single-prec.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o getrf -c 19 -g 0 -b 960 -n 67200 -v -p 2 -t

STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script-single-prec.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o getrf -c 23 -g 0 -b 960 -n 67200 -v -p 1 -t
STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script-single-prec.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o getrf -c 23 -g 0 -b 960 -n 67200 -v -p 2 -t

## double
STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o getrf -c 19 -g 4 -b 960 -n 67200 -v -p 1 -t
STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o getrf -c 19 -g 4 -b 960 -n 67200 -v -p 2 -t

STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o getrf -c 1 -g 1 -b 960 -n 67200 -v -p 1 -t
STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o getrf -c 1 -g 1 -b 960 -n 67200 -v -p 2 -t

STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o getrf -c 19 -g 0 -b 960 -n 67200 -v -p 1 -t
STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o getrf -c 19 -g 0 -b 960 -n 67200 -v -p 2 -t

STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o getrf -c 23 -g 0 -b 960 -n 67200 -v -p 1 -t
STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o getrf -c 23 -g 0 -b 960 -n 67200 -v -p 2 -t

# trace geqrf
## single
STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script-single-prec.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o geqrf -c 19 -g 4 -b 960 -n 67200 -v -p 1 -t
STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script-single-prec.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o geqrf -c 19 -g 4 -b 960 -n 67200 -v -p 2 -t

STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script-single-prec.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o geqrf -c 1 -g 1 -b 960 -n 67200 -v -p 1 -t
STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script-single-prec.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o geqrf -c 1 -g 1 -b 960 -n 67200 -v -p 2 -t

STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script-single-prec.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o geqrf -c 19 -g 0 -b 960 -n 67200 -v -p 1 -t
STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script-single-prec.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o geqrf -c 19 -g 0 -b 960 -n 67200 -v -p 2 -t

STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script-single-prec.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o geqrf -c 23 -g 0 -b 960 -n 67200 -v -p 1 -t
STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script-single-prec.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o geqrf -c 23 -g 0 -b 960 -n 67200 -v -p 2 -t
## double
STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o geqrf -c 19 -g 4 -b 960 -n 67200 -v -p 1 -t
STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o geqrf -c 19 -g 4 -b 960 -n 67200 -v -p 2 -t

STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o geqrf -c 1 -g 1 -b 960 -n 67200 -v -p 1 -t
STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o geqrf -c 1 -g 1 -b 960 -n 67200 -v -p 2 -t

STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o geqrf -c 19 -g 0 -b 960 -n 67200 -v -p 1 -t
STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o geqrf -c 19 -g 0 -b 960 -n 67200 -v -p 2 -t

STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o geqrf -c 23 -g 0 -b 960 -n 67200 -v -p 1 -t
STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o geqrf -c 23 -g 0 -b 960 -n 67200 -v -p 2 -t


# trace gemm
## single
STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script-single-prec.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o gemm -c 19 -g 4 -b 960 -n 67200 -v -p 1 -t
STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script-single-prec.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o gemm -c 19 -g 4 -b 960 -n 67200 -v -p 2 -t

STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script-single-prec.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o gemm -c 1 -g 1 -b 960 -n 67200 -v -p 1 -t
STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script-single-prec.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o gemm -c 1 -g 1 -b 960 -n 67200 -v -p 2 -t

STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script-single-prec.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o gemm -c 19 -g 0 -b 960 -n 67200 -v -p 1 -t
STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script-single-prec.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o gemm -c 19 -g 0 -b 960 -n 67200 -v -p 2 -t

STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script-single-prec.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o gemm -c 23 -g 0 -b 960 -n 67200 -v -p 1 -t
STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script-single-prec.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o gemm -c 23 -g 0 -b 960 -n 67200 -v -p 2 -t

## double
STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o gemm -c 19 -g 4 -b 960 -n 67200 -v -p 1 -t
STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o gemm -c 19 -g 4 -b 960 -n 67200 -v -p 2 -t

STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o gemm -c 1 -g 1 -b 960 -n 67200 -v -p 1 -t
STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o gemm -c 1 -g 1 -b 960 -n 67200 -v -p 2 -t

STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o gemm -c 19 -g 0 -b 960 -n 67200 -v -p 1 -t
STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o gemm -c 19 -g 0 -b 960 -n 67200 -v -p 2 -t

STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o gemm -c 23 -g 0 -b 960 -n 67200 -v -p 1 -t
STARPU_TRACE_BUFFER_SIZE=256 STARPU_FXT_TRACE=1 ./run-script.sh -r 2 -m /var/nfs_dir/hosts -j $expkey -o gemm -c 23 -g 0 -b 960 -n 67200 -v -p 2 -t
