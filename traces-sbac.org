* Cases with traces
** 2xFat
*** find exp ID
#+begin_src R :results output :session *R* :exports both :eval no-export
zAll21Trace %>%
  filter(Platform == "2xFat",
         app == "dgeqrf") %>%
  select(id, app, threads, gpus, P, n, time, gflops) %>%
  print %>%
  pull(id)
#+end_src

#+RESULTS:
: # A tibble: 2 × 8
:   id                                            app   threads  gpus     P     n  time gflops
:   <chr>                                         <chr>   <int> <int> <int> <int> <dbl>  <dbl>
: 1 nfs_dir_2xFat/wcc-07-2024/202407091714/ExpLo… dgeq…      19     4     1 67200  222.  1825.
: 2 nfs_dir_2xFat/wcc-07-2024/202407091714/ExpLo… dgeq…      19     4     2 67200 1364.   297.
: [1] "nfs_dir_2xFat/wcc-07-2024/202407091714/ExpLog-202407091714-ip-10-0-0-10.ec2.internal-07-12-2024-13h-41min-13s.org:0"
: [2] "nfs_dir_2xFat/wcc-07-2024/202407091714/ExpLog-202407091714-ip-10-0-0-10.ec2.internal-07-12-2024-13h-46min-06s.org:0"

*** starvz plot =QR_2xFat_P1=
#+begin_src R :results output :session *Rvz* :exports both :eval no-export
library(starvz)
#list.files("results/nfs_dir_2xFat/wcc-07-2024/202407091714/ExpLog-202407091714-ip-10-0-0-10.ec2.internal-07-12-2024-13h-41min-13s")
QR_2xFat_P1 <- starvz_read("results/nfs_dir_2xFat/wcc-07-2024/202407091714/ExpLog-202407091714-ip-10-0-0-10.ec2.internal-07-12-2024-13h-41min-13s",
                           selective = FALSE)
#+end_src

#+RESULTS:

#+begin_src R :results output graphics file :file fig-trace-QR-2xFat-P1.png :exports both :width 1400 :height 1800 :session *Rvz* :eval no-export
QR_2xFat_P1$config$starpu$active = TRUE
QR_2xFat_P1$config$st$labels = "FIRST_LAST"
QR_2xFat_P1$config$st$height = 8.0
QR_2xFat_P1$config$ready$active = TRUE
QR_2xFat_P1$config$ready$height = 1.0
QR_2xFat_P1$config$lackready$active = TRUE
QR_2xFat_P1$config$lackready$height = 0.15
QR_2xFat_P1$config$kiteration$active = TRUE
QR_2xFat_P1$config$kiteration$legend = FALSE
QR_2xFat_P1$config$kiteration$height = 0.9
QR_2xFat_P1$config$mpibandwidth$active = TRUE
QR_2xFat_P1$config$mpibandwidth$height = 1.2
QR_2xFat_P1$config$st$idleness = TRUE
QR_2xFat_P1$config$usedmemory$active = TRUE
QR_2xFat_P1$config$usedmemory$height = 1.2
starvz_plot(QR_2xFat_P1)
#+end_src

#+RESULTS:
[[file:fig-trace-QR-2xFat-P1.png]]

*** starvz plot =QR_2xFat_P2=
Este aqui (e outros esta dando segfault no R+starvz):
#+begin_example
03:30:33 AM Merge state data with the DAG
03:30:34 AM Get MPI tasks (links) to enrich the DAG

 *** caught segfault ***
address 0x55e43c4510fc, cause 'memory not mapped'

Traceback:
 1: separate_res(.)
 2: eval_tidy(xs[[j]], mask)
 3: tibble_quos(xs, .rows, .name_repair)
 4: tibble(.)
 5: mutate(., Resource = as.factor(.data$Resource))
 6: mutate(., Node = as.factor(.data$Node))
 7: mutate(., ResourceType = as.factor(gsub("[[:digit:]]+", "", .data$Resource)))
 8: dfdag %>% filter(grepl("mpicom", .data$JobId)) %>% full_join(dfl,     by = c(JobId = "Key")) %>% select(-"Container", -"Origin") %>%     rename(ResourceId = "Dest") %>% mutate(ResourceId = as.factor(.data$ResourceId)) %>%     separate_res() %>% tibble() %>% mutate(Resource = as.factor(.data$Resource)) %>%     mutate(Node = as.factor(.data$Node)) %>% mutate(ResourceType = as.factor(gsub("[[:digit:]]+",     "", .data$Resource)))
 9: read_dag(where = directory, Worker$Application %>% mutate(Application = TRUE),     dfl)
10: starvz_phase1(directory = input.directory, app_states_fun = states.fun,     state_filter = states.filter, whichApplication = input.application,     input.parquet = input.parquet, config = config)
An irrecoverable exception occurred. R is aborting now ...
./phase1-workflow.sh: line 238: 25320 Segmentation fault      (core dumped) phase1-workflow.R . ${APPLICATION} ${3:-}
Error when executing phase1-workflow.R (exit status: 139)
Phase 1 Failed (exit status: 2) stopping
#+end_example

#+begin_src R :results output :session *Rvz* :exports both :eval no-export
library(starvz)
#list.files("results/nfs_dir_2xFat/wcc-07-2024/202407091714/ExpLog-202407091714-ip-10-0-0-10.ec2.internal-07-12-2024-13h-41min-13s")
QR_2xFat_P2 <- starvz_read("results/nfs_dir_2xFat/wcc-07-2024/202407091714/ExpLog-202407091714-ip-10-0-0-10.ec2.internal-07-12-2024-13h-46min-06s",
                           selective = FALSE)
#+end_src

#+RESULTS:

#+begin_src R :results output graphics file :file (org-babel-temp-file "figure" ".png") :exports both :width 1400 :height 900 :session *Rvz* :eval no-export
QR_2xFat_P2$config$st$labels = "FIRST_LAST"
QR_2xFat_P2$config$ready$active = TRUE
QR_2xFat_P2$config$ready$height = 1.0
QR_2xFat_P2$config$lackready$active = TRUE
QR_2xFat_P2$config$lackready$height = 0.15
QR_2xFat_P2$config$kiteration$active = TRUE
QR_2xFat_P2$config$kiteration$legend = FALSE
QR_2xFat_P2$config$kiteration$height = 0.9
QR_2xFat_P2$config$mpibandwidth$active = TRUE
QR_2xFat_P2$config$mpibandwidth$height = 1.2
QR_2xFat_P2$config$st$idleness = TRUE
QR_2xFat_P2$config$usedmemory$active = TRUE
QR_2xFat_P2$config$usedmemory$height = 1.2
starvz_plot(QR_2xFat_P2)
#+end_src

#+RESULTS:
[[file:/tmp/babel-dp4ZyM/figureyBc8OY.png]]

** 8xThin
*** find exp ID
#+begin_src R :results output :session *R* :exports both :eval no-export
zAll21Trace %>%
  filter(Platform == "8xThin",
         threads == 2, gpus == 1,
         app == "dgeqrf") %>%
  select(id, app, threads, gpus, P, n, time, gflops) %>%
  print %>%
  pull(id)
#+end_src

#+RESULTS:
#+begin_example
# A tibble: 4 × 8
  id                                            app   threads  gpus     P     n  time gflops
  <chr>                                         <chr>   <int> <int> <int> <int> <dbl>  <dbl>
1 nfs_dir_8xThin/wcc-07-2024/202407111715/ExpL… dgeq…       2     1     1 67200  232.  1745.
2 nfs_dir_8xThin/wcc-07-2024/202407111715/ExpL… dgeq…       2     1     2 67200  356.  1136.
3 nfs_dir_8xThin/wcc-07-2024/202407111715/ExpL… dgeq…       2     1     8 67200  256.  1580.
4 nfs_dir_8xThin/wcc-07-2024/202407111715/ExpL… dgeq…       2     1     4 67200  257.  1573.
[1] "nfs_dir_8xThin/wcc-07-2024/202407111715/ExpLog-202407111715-ip-10-0-0-10.ec2.internal-07-14-2024-15h-49min-51s.org:0"
[2] "nfs_dir_8xThin/wcc-07-2024/202407111715/ExpLog-202407111715-ip-10-0-0-10.ec2.internal-07-14-2024-15h-54min-29s.org:0"
[3] "nfs_dir_8xThin/wcc-07-2024/202407111715/ExpLog-202407111715-ip-10-0-0-10.ec2.internal-07-15-2024-00h-45min-12s.org:0"
[4] "nfs_dir_8xThin/wcc-07-2024/202407111715/ExpLog-202407111715-ip-10-0-0-10.ec2.internal-07-15-2024-00h-50min-21s.org:0"
#+end_example

